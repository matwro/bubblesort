import java.util.Scanner;

public class BubbleSort {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of elements in the list: ");
        int n = scanner.nextInt();

        int[] baseArray = new int[n];

        System.out.println("Enter the elements of the list:");
        for (int i = 0; i < n; i++) {
            baseArray[i] = scanner.nextInt();
        }

        int[] sortedArray = baseArray.clone();

        sortArray(sortedArray);
        System.out.println("Original list: ");
        printArray(baseArray);

        System.out.println(" ");
        System.out.println("Sorted list");
        printArray(sortedArray);
        scanner.close();
    }

    static void printArray(int[] array) {
        for (int value : array) {
            System.out.print(value + " ");
        }
    }
    static void sortArray(int[] array){
        for(int i=0; i<array.length;i ++){
            for(int j=0; j< array.length-i-1;j++){
                if (array[j] > array[j+1]){
                    int x = array[j];
                    array[j]=array[j+1];
                    array[j+1]=x;
                }
            }
        }
    }
}
